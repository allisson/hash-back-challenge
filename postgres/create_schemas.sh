#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -d "hash" <<-EOSQL
    CREATE TABLE IF NOT EXISTS "products" (
		"id" VARCHAR(64) PRIMARY KEY NOT NULL,
		"title" VARCHAR(128) NOT NULL,
		"description" TEXT NOT NULL,
		"price_in_cents" INT NOT NULL,
		"created_at" TIMESTAMPTZ NOT NULL DEFAULT NOW()
	);
    CREATE TABLE IF NOT EXISTS "users" (
		"id" VARCHAR(64) PRIMARY KEY NOT NULL,
		"first_name" VARCHAR(32) NOT NULL,
		"last_name" VARCHAR(64) NOT NULL,
		"date_of_birth" DATE NOT NULL,
		"created_at" TIMESTAMPTZ NOT NULL DEFAULT NOW()
	);
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -d "hash_test" <<-EOSQL
    CREATE TABLE IF NOT EXISTS "products" (
		"id" VARCHAR(64) PRIMARY KEY NOT NULL,
		"title" VARCHAR(128) NOT NULL,
		"description" TEXT NOT NULL,
		"price_in_cents" INT NOT NULL,
		"created_at" TIMESTAMPTZ NOT NULL DEFAULT NOW()
	);
    CREATE TABLE IF NOT EXISTS "users" (
		"id" VARCHAR(64) PRIMARY KEY NOT NULL,
		"first_name" VARCHAR(32) NOT NULL,
		"last_name" VARCHAR(64) NOT NULL,
		"date_of_birth" DATE NOT NULL,
		"created_at" TIMESTAMPTZ NOT NULL DEFAULT NOW()
	);
EOSQL

service1-test:
	docker-compose run --rm service1 make test

service2-test:
	docker-compose run --rm service2 make test

service2-lint:
	docker-compose run --rm service2 make lint

service2-mock:
	docker-compose run --rm service2 make mock

# hash-back-challenge

## Informações iniciais

Temos quatro diretórios na raíz do repositório:
* postgres: Contém a configuração do postgres, criação de databases e schemas.
* protodef: Definição do serviço grpc
* service1: Servidor grpc escrito em python
* service2: Servidor http escrito em go

## Como iniciar o projeto

```bash
docker-compose up
```

Isso vai iniciar:
* Postgresql na porta 5432 do host
* Service1 na porta 50051 do host
* Service2 na porta 8080 do host

Depois de iniciado, podemos rodar os testes com os comandos:

```bash
make service1-test
```
```bash
make service2-test
```

No `docker-compose.yml` tem as opções de volumes e commands comentadas, basta ativar para ter o mapeamento do código nos containers docker.

## Criando usuário e produto

```bash
curl -X POST \
  http://localhost:8080/users \
  -H 'Content-Type: application/json' \
  -d '{
	"first_name": "John",
	"last_name": "Connor",
	"date_of_birth": "1985-03-14"
}'
```
```bash
curl -X POST \
  http://localhost:8080/products \
  -H 'Content-Type: application/json' \
  -d '{
	"title": "My Product",
	"description": "My Description",
	"price_in_cents": 10000
}'
```

## Obtendo a lista de usuários e produtos

```bash
curl -X GET \
  http://localhost:8080/users
```
```bash
curl -X GET \
  http://localhost:8080/products
```

## Obtendo a lista de produtos com o usuário identificado

```bash
curl -X GET \
  http://localhost:8080/products \
  -H 'X-USER-ID: 09f8c00a-6ef5-4cf4-a3ba-56e4fbc509d9'
```

## Determinando o dia da black friday

Basta alterar a envvar `BLACK_FRIDAY_DATE` no `docker-compose.yml` e colocar a data corrente.

module github.com/allisson/service2

require (
	github.com/DATA-DOG/go-txdb v0.1.2
	github.com/golang/protobuf v1.3.0
	github.com/google/uuid v1.1.1
	github.com/huandu/go-sqlbuilder v1.1.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/lpar/calendar v0.2.0
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.3.0
	github.com/urfave/negroni v1.0.0
	github.com/xeipuuv/gojsonpointer v0.0.0-20180127040702-4e3ac2762d5f // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	github.com/xeipuuv/gojsonschema v1.1.0
	github.com/xujiajun/gorouter v1.2.0
	google.golang.org/grpc v1.19.0
)

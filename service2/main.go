package main

import (
	"log"
	"net/http"
	"os"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc"

	pb "github.com/allisson/service2/protobuffer"
	"github.com/allisson/service2/repository"
	"github.com/allisson/service2/service"
	"github.com/allisson/service2/web"
	_ "github.com/lib/pq"
	"github.com/urfave/negroni"
	"github.com/xujiajun/gorouter"
)

var db *sqlx.DB
var gc *grpc.ClientConn

func init() {
	dbConn, err := sqlx.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Panic(err)
	}
	db = dbConn
	gConn, err := grpc.Dial("service1:50051", grpc.WithInsecure())
	if err != nil {
		log.Panic(err)
	}
	gc = gConn
}

func main() {
	mux := gorouter.New()

	productRepo := repository.Product{DB: db}
	discountRepo := repository.Discount{Client: pb.NewDiscountClient(gc)}
	productService := service.Product{ProductRepo: &productRepo, DiscountRepo: &discountRepo}
	productHandler := web.ProductHandler{ProductService: &productService}
	mux.GET("/products", productHandler.List)
	mux.POST("/products", productHandler.Create)

	userRepo := repository.User{DB: db}
	userService := service.User{UserRepo: &userRepo}
	userHandler := web.UserHandler{UserService: &userService}
	mux.GET("/users", userHandler.List)
	mux.POST("/users", userHandler.Create)

	n := negroni.Classic()
	n.UseHandler(mux)
	log.Fatal(http.ListenAndServe(":8080", n))
}

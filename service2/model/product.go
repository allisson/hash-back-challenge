package model

import "time"

// Discount data
type Discount struct {
	Pct          float32 `json:"pct"`
	ValueInCents int     `json:"value_in_cents"`
}

// Product data
type Product struct {
	ID           string    `json:"id" db:"id"`
	Title        string    `json:"title" db:"title"`
	Description  string    `json:"description" db:"description"`
	PriceInCents int       `json:"price_in_cents" db:"price_in_cents"`
	Discount     *Discount `json:"discount"`
	CreatedAt    time.Time `json:"created_at" db:"created_at"`
}

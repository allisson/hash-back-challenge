package model

import (
	"time"

	"github.com/lpar/calendar"
)

// User data
type User struct {
	ID          string        `json:"id" db:"id"`
	FirstName   string        `json:"first_name" db:"first_name"`
	LastName    string        `json:"last_name" db:"last_name"`
	DateOfBirth calendar.Date `json:"date_of_birth" db:"date_of_birth"`
	CreatedAt   time.Time     `json:"created_at" db:"created_at"`
}

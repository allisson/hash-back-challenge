package repository

import (
	"github.com/allisson/service2/model"
	"github.com/huandu/go-sqlbuilder"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

// Product implements ProductRepository interface
type Product struct {
	DB *sqlx.DB
}

// FindAll returns all products by limit and offset
func (r *Product) FindAll() ([]*model.Product, error) {
	sb := sqlbuilder.PostgreSQL.NewSelectBuilder()
	sb.Select("*").From("products")
	sql, args := sb.Build()
	products := []*model.Product{}
	err := r.DB.Select(&products, sql, args...)
	if err != nil {
		err = errors.Wrapf(err, "struct=repository.Product, method=FindAll")
	}
	return products, err
}

// Store create/update product
func (r *Product) Store(product *model.Product) error {
	sb := sqlbuilder.PostgreSQL.NewInsertBuilder()
	sb.InsertInto("products").Cols(
		"id",
		"price_in_cents",
		"title",
		"description",
		"created_at",
	).Values(
		product.ID,
		product.PriceInCents,
		product.Title,
		product.Description,
		product.CreatedAt,
	)
	sql, args := sb.Build()
	_, err := r.DB.Exec(sql, args...)
	if err != nil {
		err = errors.Wrapf(err, "struct=repository.Product, method=create, product=%#v", product)
	}
	return err
}

package repository

import (
	"context"

	"github.com/allisson/service2/model"
	pb "github.com/allisson/service2/protobuffer"
	"github.com/pkg/errors"
)

// Discount implements DiscounRepository interface
type Discount struct {
	Client pb.DiscountClient
}

// Fetch return model.Discount from grpc server
func (r *Discount) Fetch(userID, productID string) (*model.Discount, error) {
	discount := model.Discount{}
	request := pb.Request{ProductId: productID, UserId: userID}
	response, err := r.Client.Fetch(context.Background(), &request)
	if err != nil {
		err = errors.Wrapf(err, "struct=repository.Discount, method=Fetch, userID=%v, productID=%v", userID, productID)
		return &discount, err
	}
	discount.Pct = response.GetPct()
	discount.ValueInCents = int(response.GetValueInCents())
	return &discount, nil
}

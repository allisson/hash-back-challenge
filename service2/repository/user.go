package repository

import (
	"github.com/allisson/service2/model"
	"github.com/huandu/go-sqlbuilder"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

// User implements UserRepository interface
type User struct {
	DB *sqlx.DB
}

// FindAll returns all users by limit and offset
func (r *User) FindAll() ([]*model.User, error) {
	sb := sqlbuilder.PostgreSQL.NewSelectBuilder()
	sb.Select("*").From("users")
	sql, args := sb.Build()
	users := []*model.User{}
	err := r.DB.Select(&users, sql, args...)
	if err != nil {
		err = errors.Wrapf(err, "struct=repository.User, method=FindAll")
	}
	return users, err
}

// Store create/update user
func (r *User) Store(user *model.User) error {
	sb := sqlbuilder.PostgreSQL.NewInsertBuilder()
	sb.InsertInto("users").Cols(
		"id",
		"first_name",
		"last_name",
		"date_of_birth",
		"created_at",
	).Values(
		user.ID,
		user.FirstName,
		user.LastName,
		user.DateOfBirth,
		user.CreatedAt,
	)
	sql, args := sb.Build()
	_, err := r.DB.Exec(sql, args...)
	if err != nil {
		err = errors.Wrapf(err, "struct=repository.User, method=create, user=%#v", user)
	}
	return err
}

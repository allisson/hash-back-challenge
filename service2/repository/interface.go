package repository

import "github.com/allisson/service2/model"

// ProductRepository interface definition
type ProductRepository interface {
	FindAll() ([]*model.Product, error)
	Store(product *model.Product) error
}

// UserRepository interface definition
type UserRepository interface {
	FindAll() ([]*model.User, error)
	Store(user *model.User) error
}

// DiscountRepository interface definition
type DiscountRepository interface {
	Fetch(userID, productID string) (*model.Discount, error)
}

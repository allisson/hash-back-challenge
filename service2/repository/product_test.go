package repository

import (
	"testing"

	"github.com/allisson/service2/testutil"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
)

func makeProductRepository(db *sqlx.DB) Product {
	return Product{DB: db}
}

func TestProductRepository(t *testing.T) {
	t.Run("Test Store", func(t *testing.T) {
		db := testutil.GetTxDB()

		productRepository := makeProductRepository(db)
		product := testutil.MakeProduct()

		err := productRepository.Store(&product)
		assert.Nil(t, err)

		db.Close()
	})

	t.Run("Test FindAll", func(t *testing.T) {
		db := testutil.GetTxDB()

		productRepository := makeProductRepository(db)
		product1 := testutil.MakeProduct()
		product2 := testutil.MakeProduct()
		err := productRepository.Store(&product1)
		assert.Nil(t, err)
		err = productRepository.Store(&product2)
		assert.Nil(t, err)

		products, err := productRepository.FindAll()
		assert.Nil(t, err)
		assert.Len(t, products, 2)

		db.Close()
	})
}

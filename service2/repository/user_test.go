package repository

import (
	"testing"

	"github.com/allisson/service2/testutil"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
)

func makeUserRepository(db *sqlx.DB) User {
	return User{DB: db}
}

func TestUserRepository(t *testing.T) {
	t.Run("Test Store", func(t *testing.T) {
		db := testutil.GetTxDB()

		userRepository := makeUserRepository(db)
		user := testutil.MakeUser()

		// Create user
		err := userRepository.Store(&user)
		assert.Nil(t, err)

		db.Close()
	})

	t.Run("Test FindAll", func(t *testing.T) {
		db := testutil.GetTxDB()

		userRepository := makeUserRepository(db)
		user1 := testutil.MakeUser()
		user2 := testutil.MakeUser()
		err := userRepository.Store(&user1)
		assert.Nil(t, err)
		err = userRepository.Store(&user2)
		assert.Nil(t, err)

		users, err := userRepository.FindAll()
		assert.Nil(t, err)
		assert.Len(t, users, 2)

		db.Close()
	})
}

package repository

import (
	"context"
	"testing"

	"github.com/allisson/service2/mocks"
	"github.com/allisson/service2/model"
	pb "github.com/allisson/service2/protobuffer"
	"github.com/stretchr/testify/assert"
)

func TestDiscountRepository(t *testing.T) {
	t.Run("Test Fetch", func(t *testing.T) {
		productID := "1"
		userID := "1"
		client := mocks.DiscountClient{}
		discountRepository := Discount{Client: &client}
		request := pb.Request{ProductId: productID, UserId: userID}
		response := pb.Response{Pct: 0.1, ValueInCents: 5000}
		expectedDiscount := model.Discount{Pct: 0.1, ValueInCents: 5000}
		client.On("Fetch", context.Background(), &request).Return(&response, nil)
		discount, err := discountRepository.Fetch(userID, productID)
		assert.Nil(t, err)
		assert.Equal(t, expectedDiscount, *discount)
	})
}

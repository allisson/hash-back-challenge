package web

import (
	"log"
	"net/http"
)

func makeResponse(w http.ResponseWriter, body string, statusCode int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	_, err := w.Write([]byte(body))
	if err != nil {
		log.Println(err)
	}
}

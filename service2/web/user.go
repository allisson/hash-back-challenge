package web

import (
	"encoding/json"
	"log"
	"net/http"

	"io/ioutil"

	"github.com/allisson/service2/model"
	"github.com/allisson/service2/service"
)

const (
	// UserSchema is a json schema for user
	UserSchema = `
	{
		"$schema": "http://json-schema.org/draft-07/schema#",
		"type": "object",
		"properties": {
			"id": {
				"type": "string",
  				"maxLength": 64
			},
			"first_name": {
				"type": "string",
				"minLength": 3,
				"maxLength": 32
			},
			"last_name": {
				"type": "string",
				"minLength": 3,
				"maxLength": 64
			},
			"date_of_birth": {
				"type": "string",
				"format": "date"
			}
		},
		"required": [
			"first_name",
			"last_name",
			"date_of_birth"
		]
	}
	`
)

// UserHandler responds http requests
type UserHandler struct {
	UserService service.UserService
}

// Create a new user
func (h *UserHandler) Create(w http.ResponseWriter, r *http.Request) {
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		makeResponse(w, `{"detail": "Unable to read request body"}`, http.StatusBadRequest)
		return
	}
	user := model.User{}
	err = RequestDecoder(requestBody, UserSchema, &user)
	if err != nil {
		log.Println(err)
		makeResponse(w, `{"detail": "Invalid Request Body"}`, http.StatusBadRequest)
		return
	}
	err = h.UserService.Create(&user)
	if err != nil {
		log.Println(err)
		makeResponse(w, `{"detail": "Internal Server Error"}`, http.StatusInternalServerError)
		return
	}
	responseBody, err := json.Marshal(&user)
	if err != nil {
		log.Println(err)
		makeResponse(w, `{"detail": "Internal Server Error"}`, http.StatusInternalServerError)
		return
	}
	makeResponse(w, string(responseBody), http.StatusCreated)
}

// List users
func (h *UserHandler) List(w http.ResponseWriter, r *http.Request) {
	users, err := h.UserService.List()
	if err != nil {
		log.Println(err)
		makeResponse(w, `{"detail": "Internal Server Error"}`, http.StatusInternalServerError)
		return
	}
	responseBody, err := json.Marshal(&users)
	if err != nil {
		log.Println(err)
		makeResponse(w, `{"detail": "Internal Server Error"}`, http.StatusInternalServerError)
		return
	}
	makeResponse(w, string(responseBody), http.StatusOK)
}

package web

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/allisson/service2/mocks"
	"github.com/allisson/service2/model"
	"github.com/allisson/service2/testutil"
	"github.com/stretchr/testify/assert"
)

func TestUserHandler(t *testing.T) {
	t.Run("Test Create", func(t *testing.T) {
		user := testutil.MakeUser()
		requestPayload, _ := json.Marshal(&user)
		service := mocks.UserService{}
		service.On("Create", &user).Return(nil)
		userHandler := UserHandler{UserService: &service}
		req, err := http.NewRequest("POST", "/users", bytes.NewReader(requestPayload))
		assert.Nil(t, err)
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(userHandler.Create)
		handler.ServeHTTP(rr, req)
		assert.Equal(t, http.StatusCreated, rr.Code)
	})

	t.Run("Test List", func(t *testing.T) {
		user1 := testutil.MakeUser()
		user2 := testutil.MakeUser()
		users := []*model.User{&user1, &user2}
		service := mocks.UserService{}
		service.On("List").Return(users, nil)
		userHandler := UserHandler{UserService: &service}
		req, err := http.NewRequest("GET", "/users", nil)
		assert.Nil(t, err)
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(userHandler.List)
		handler.ServeHTTP(rr, req)
		assert.Equal(t, http.StatusOK, rr.Code)
	})
}

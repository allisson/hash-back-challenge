package web

import (
	"encoding/json"
	"errors"
	"log"

	"github.com/xeipuuv/gojsonschema"
)

// RequestDecoder validate request payload against json schema and decode json to struct
func RequestDecoder(requestBody []byte, schema string, model interface{}) error {
	defaultErr := errors.New("Invalid request body")
	schemaLoader := gojsonschema.NewStringLoader(schema)
	documentLoader := gojsonschema.NewStringLoader(string(requestBody))
	result, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		log.Println(err)
		return defaultErr
	}
	if !result.Valid() {
		return defaultErr
	}
	if err := json.Unmarshal(requestBody, &model); err != nil {
		log.Println(err)
		return defaultErr
	}
	return nil
}

package web

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/allisson/service2/model"
	"github.com/allisson/service2/service"
)

const (
	// ProductSchema is a json schema for product
	ProductSchema = `
	{
		"$schema": "http://json-schema.org/draft-07/schema#",
		"type": "object",
		"properties": {
			"id": {
				"type": "string",
  				"maxLength": 64
			},
			"title": {
				"type": "string",
				"minLength": 3,
				"maxLength": 128
			},
			"description": {
				"type": "string",
				"maxLength": 4000
			},
			"price_in_cents": {
				"type": "integer",
				"minLength": 100,
				"maxLength": 999999
			}
		},
		"required": [
			"title",
			"description",
			"price_in_cents"
		]
	}
	`
)

// ProductHandler responds http requests
type ProductHandler struct {
	ProductService service.ProductService
}

// Create a new product
func (h *ProductHandler) Create(w http.ResponseWriter, r *http.Request) {
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		makeResponse(w, `{"detail": "Unable to read request body"}`, http.StatusBadRequest)
		return
	}
	product := model.Product{}
	err = RequestDecoder(requestBody, ProductSchema, &product)
	if err != nil {
		log.Println(err)
		makeResponse(w, `{"detail": "Invalid Request Body"}`, http.StatusBadRequest)
		return
	}
	err = h.ProductService.Create(&product)
	if err != nil {
		log.Println(err)
		makeResponse(w, `{"detail": "Internal Server Error"}`, http.StatusInternalServerError)
		return
	}
	responseBody, err := json.Marshal(&product)
	if err != nil {
		log.Println(err)
		makeResponse(w, `{"detail": "Internal Server Error"}`, http.StatusInternalServerError)
		return
	}
	makeResponse(w, string(responseBody), http.StatusCreated)
}

// List products
func (h *ProductHandler) List(w http.ResponseWriter, r *http.Request) {
	userID := r.Header.Get("X-USER-ID")
	products, err := h.ProductService.List(userID)
	if err != nil {
		log.Println(err)
		makeResponse(w, `{"detail": "Internal Server Error"}`, http.StatusInternalServerError)
		return
	}
	responseBody, err := json.Marshal(&products)
	if err != nil {
		log.Println(err)
		makeResponse(w, `{"detail": "Internal Server Error"}`, http.StatusInternalServerError)
		return
	}
	makeResponse(w, string(responseBody), http.StatusOK)
}

package web

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

type Counter struct {
	Total int `json:"total"`
}

const (
	// PUTSchema is a json schema for update
	TestSchema = `
	{
		"$schema": "http://json-schema.org/draft-07/schema#",
		"type": "object",
		"properties": {
			"total": {"type": "integer"}
		},
		"required": [
			"total"
		]
	}
	`
)

func TestRequestDecoder(t *testing.T) {
	t.Run("Test with invalid requestBody", func(t *testing.T) {
		model := Counter{}
		requestBody := `{`
		err := RequestDecoder([]byte(requestBody), TestSchema, &model)
		expectedError := errors.New("Invalid request body")
		assert.Equal(t, expectedError, err)
	})

	t.Run("Test with empty requestBody", func(t *testing.T) {
		model := Counter{}
		requestBody := `{}`
		err := RequestDecoder([]byte(requestBody), TestSchema, &model)
		expectedError := errors.New("Invalid request body")
		assert.Equal(t, expectedError, err)
	})

	t.Run("Test with valid requestBody", func(t *testing.T) {
		model := Counter{}
		requestBody := `{"total": 1}`
		err := RequestDecoder([]byte(requestBody), TestSchema, &model)
		assert.Nil(t, err)
		assert.Equal(t, 1, model.Total)
	})
}

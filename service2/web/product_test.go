package web

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/allisson/service2/mocks"
	"github.com/allisson/service2/model"
	"github.com/allisson/service2/testutil"
	"github.com/stretchr/testify/assert"
)

func TestProductHandler(t *testing.T) {
	t.Run("Test Create", func(t *testing.T) {
		product := testutil.MakeProduct()
		requestPayload, _ := json.Marshal(&product)
		service := mocks.ProductService{}
		service.On("Create", &product).Return(nil)
		productHandler := ProductHandler{ProductService: &service}
		req, err := http.NewRequest("POST", "/products", bytes.NewReader(requestPayload))
		assert.Nil(t, err)
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(productHandler.Create)
		handler.ServeHTTP(rr, req)
		assert.Equal(t, http.StatusCreated, rr.Code)
	})

	t.Run("Test List", func(t *testing.T) {
		product1 := testutil.MakeProduct()
		product2 := testutil.MakeProduct()
		products := []*model.Product{&product1, &product2}
		service := mocks.ProductService{}
		service.On("List", "userID").Return(products, nil)
		productHandler := ProductHandler{ProductService: &service}
		req, err := http.NewRequest("GET", "/products", nil)
		assert.Nil(t, err)
		req.Header.Set("X-USER-ID", "userID")
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(productHandler.List)
		handler.ServeHTTP(rr, req)
		assert.Equal(t, http.StatusOK, rr.Code)
	})
}

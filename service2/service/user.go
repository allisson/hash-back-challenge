package service

import (
	"time"

	"github.com/allisson/service2/model"
	"github.com/allisson/service2/repository"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// User implements UserService interface
type User struct {
	UserRepo repository.UserRepository
}

// Create new User
func (s *User) Create(user *model.User) error {
	if user.ID == "" {
		id, _ := uuid.NewRandom()
		user.ID = id.String()
	}
	user.CreatedAt = time.Now().UTC()
	err := s.UserRepo.Store(user)
	if err != nil {
		err = errors.Wrapf(err, "struct=service.User, method=Create, user=%#v", user)
	}
	return err
}

// List returns Users from database
func (s *User) List() ([]*model.User, error) {
	users, err := s.UserRepo.FindAll()
	if err != nil {
		err = errors.Wrapf(err, "struct=service.User, method=List")
	}
	return users, err
}

package service

import (
	"log"
	"time"

	"github.com/allisson/service2/model"
	"github.com/allisson/service2/repository"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// Product implements ProductService interface
type Product struct {
	ProductRepo  repository.ProductRepository
	DiscountRepo repository.DiscountRepository
}

// Create new product
func (s *Product) Create(product *model.Product) error {
	if product.ID == "" {
		id, _ := uuid.NewRandom()
		product.ID = id.String()
	}
	product.CreatedAt = time.Now().UTC()
	err := s.ProductRepo.Store(product)
	if err != nil {
		err = errors.Wrapf(err, "struct=service.Product, method=Create, product=%#v", product)
	}
	return err
}

// List returns products from database
func (s *Product) List(userID string) ([]*model.Product, error) {
	products, err := s.ProductRepo.FindAll()
	if err != nil {
		err = errors.Wrapf(err, "struct=service.Product, method=List, userID=%v", userID)
	}
	for _, product := range products {
		discount, err := s.DiscountRepo.Fetch(userID, product.ID)
		if err != nil {
			log.Println(err)
		} else {
			product.Discount = discount
		}
	}
	return products, err
}

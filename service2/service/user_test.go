package service

import (
	"testing"

	"github.com/allisson/service2/model"

	"github.com/allisson/service2/testutil"
	"github.com/stretchr/testify/assert"

	"github.com/allisson/service2/mocks"
)

func TestUserService(t *testing.T) {
	t.Run("Test Create", func(t *testing.T) {
		user := testutil.MakeUser()
		user.ID = ""
		createdAt := user.CreatedAt
		repo := mocks.UserRepository{}
		service := User{UserRepo: &repo}
		repo.On("Store", &user).Return(nil)
		err := service.Create(&user)
		assert.Nil(t, err)
		assert.NotEqual(t, "", user.ID)
		assert.NotEqual(t, createdAt, user.CreatedAt)
	})

	t.Run("Test List", func(t *testing.T) {
		user1 := testutil.MakeUser()
		user2 := testutil.MakeUser()
		users := []*model.User{&user1, &user2}
		repo := mocks.UserRepository{}
		service := User{UserRepo: &repo}
		repo.On("FindAll").Return(users, nil)
		usersFromDB, err := service.List()
		assert.Nil(t, err)
		assert.Equal(t, users, usersFromDB)
	})
}

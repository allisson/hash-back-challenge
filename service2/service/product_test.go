package service

import (
	"testing"

	"github.com/allisson/service2/model"

	"github.com/allisson/service2/testutil"
	"github.com/stretchr/testify/assert"

	"github.com/allisson/service2/mocks"
)

func TestProductService(t *testing.T) {
	t.Run("Test Create", func(t *testing.T) {
		product := testutil.MakeProduct()
		product.ID = ""
		createdAt := product.CreatedAt
		productRepo := mocks.ProductRepository{}
		service := Product{ProductRepo: &productRepo}
		productRepo.On("Store", &product).Return(nil)
		err := service.Create(&product)
		assert.Nil(t, err)
		assert.NotEqual(t, "", product.ID)
		assert.NotEqual(t, createdAt, product.CreatedAt)
	})

	t.Run("Test List", func(t *testing.T) {
		discount := model.Discount{Pct: 0.1, ValueInCents: 4500}
		product1 := testutil.MakeProduct()
		product2 := testutil.MakeProduct()
		products := []*model.Product{&product1, &product2}
		productRepo := mocks.ProductRepository{}
		discountRepo := mocks.DiscountRepository{}
		service := Product{ProductRepo: &productRepo, DiscountRepo: &discountRepo}
		productRepo.On("FindAll").Return(products, nil)
		discountRepo.On("Fetch", "userID", products[0].ID).Return(&discount, nil)
		discountRepo.On("Fetch", "userID", products[1].ID).Return(&discount, nil)
		productsFromDB, err := service.List("userID")
		assert.Nil(t, err)
		assert.Equal(t, products, productsFromDB)
		assert.NotNil(t, productsFromDB[0].Discount)
	})
}

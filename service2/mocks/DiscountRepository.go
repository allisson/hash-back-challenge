// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"
import model "github.com/allisson/service2/model"

// DiscountRepository is an autogenerated mock type for the DiscountRepository type
type DiscountRepository struct {
	mock.Mock
}

// Fetch provides a mock function with given fields: userID, productID
func (_m *DiscountRepository) Fetch(userID string, productID string) (*model.Discount, error) {
	ret := _m.Called(userID, productID)

	var r0 *model.Discount
	if rf, ok := ret.Get(0).(func(string, string) *model.Discount); ok {
		r0 = rf(userID, productID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*model.Discount)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, string) error); ok {
		r1 = rf(userID, productID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

package testutil

import (
	"time"

	"github.com/allisson/service2/model"
	"github.com/google/uuid"
)

// MakeProduct return a filled Product
func MakeProduct() model.Product {
	id, _ := uuid.NewRandom()
	return model.Product{
		ID:           id.String(),
		Title:        "My Product",
		Description:  "My Description",
		PriceInCents: 10000,
		CreatedAt:    time.Now().UTC(),
	}
}

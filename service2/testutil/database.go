package testutil

import (
	"os"

	"github.com/DATA-DOG/go-txdb"
	"github.com/jmoiron/sqlx"

	// Load postgres driver for txdb
	_ "github.com/lib/pq"
)

func init() {
	txdb.Register("txdb", "postgres", os.Getenv("TEST_DATABASE_URL"))
}

// GetTxDB returns *sqlx.DB powered by txdb
func GetTxDB() *sqlx.DB {
	db, _ := sqlx.Open("txdb", "identifier")
	return db
}

package testutil

import (
	"time"

	"github.com/allisson/service2/model"
	"github.com/google/uuid"
	"github.com/lpar/calendar"
)

// MakeUser return a filled User
func MakeUser() model.User {
	id, _ := uuid.NewRandom()
	return model.User{
		ID:          id.String(),
		FirstName:   "John",
		LastName:    "Connor",
		DateOfBirth: calendar.NewDate(1985, 2, 28),
		CreatedAt:   time.Now().UTC(),
	}
}

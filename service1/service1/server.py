import time
from concurrent import futures

import grpc

from .pb.discount_pb2_grpc import add_DiscountServicer_to_server
from .service import DiscountService

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    add_DiscountServicer_to_server(DiscountService(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

import os
from datetime import date, datetime

from .exceptions import ProductNotFoundError, UserNotFoundError


def user_birthday_action(today, user, product, discount):
    conditions = (
        today.day == user['date_of_birth'].day,
        today.month == user['date_of_birth'].month
    )
    if all(conditions):
        discount['pct'] += 0.05
        discount['value_in_cents'] = int(product['price_in_cents'] - product['price_in_cents'] * discount['pct'])


def black_friday_action(today, user, product, discount):
    black_friday_str = os.environ.get('BLACK_FRIDAY_DATE', '2019-11-25')
    black_friday_date = datetime.strptime(black_friday_str, '%Y-%m-%d').date()
    conditions = (
        today.day == black_friday_date.day,
        today.month == black_friday_date.month
    )
    if all(conditions):
        discount['pct'] += 0.1
        discount['value_in_cents'] = int(product['price_in_cents'] - product['price_in_cents'] * discount['pct'])


def max_discount_action(today, user, product, discount):
    if discount['pct'] > 0.1:
        discount['pct'] = 0.1
        discount['value_in_cents'] = int(product['price_in_cents'] - product['price_in_cents'] * discount['pct'])


class DiscountPipeline:
    actions = (
        user_birthday_action,
        black_friday_action,
        max_discount_action,
    )

    def __init__(self, user_id, product_id, database_manager):
        self.user_id = user_id
        self.product_id = product_id
        self.database_manager = database_manager

    def run(self):
        user = self.database_manager.fetch_user(self.user_id)
        if not user:
            raise UserNotFoundError('user_not_found, user_id={}'.format(self.user_id))

        product = self.database_manager.fetch_product(self.product_id)
        if not product:
            raise ProductNotFoundError('product_not_found, product_id={}'.format(self.product_id))

        discount = {'pct': 0, 'value_in_cents': product['price_in_cents']}
        today = date.today()

        for action in self.actions:
            action(today, user, product, discount)

        return discount

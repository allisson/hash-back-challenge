class UserNotFoundError(Exception):
    pass


class ProductNotFoundError(Exception):
    pass

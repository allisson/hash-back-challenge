from contextlib import suppress

import records
from cached_property import threaded_cached_property


class DatabaseManager:
    def __init__(self, database_url):
        self.database_url = database_url

    @threaded_cached_property
    def db(self):
        return records.Database(self.database_url)

    def fetch_user(self, id):
        user = {}

        row = self.db.query(
            '''
            SELECT * FROM users
            WHERE id = :id
            ''',
            id=id
        )

        with suppress(IndexError):
            user = row[0].as_dict()

        return user

    def fetch_product(self, id):
        product = {}

        row = self.db.query(
            '''
            SELECT * FROM products
            WHERE id = :id
            ''',
            id=id
        )

        with suppress(IndexError):
            product = row[0].as_dict()

        return product

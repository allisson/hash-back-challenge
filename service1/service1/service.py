import logging
import os

import grpc

from .database import DatabaseManager
from .exceptions import ProductNotFoundError, UserNotFoundError
from .pb.discount_pb2 import Response
from .pb.discount_pb2_grpc import DiscountServicer
from .pipeline import DiscountPipeline

logger = logging.getLogger(__name__)


class DiscountService(DiscountServicer):
    def __init__(self):
        self.database_manager = DatabaseManager(database_url=os.environ.get('DATABASE_URL'))

    def Fetch(self, request, context):
        logger.info('request_received, request={!r}'.format(request, context))
        discount_pipeline = DiscountPipeline(request.user_id, request.product_id, self.database_manager)

        try:
            discount = discount_pipeline.run()
        except (UserNotFoundError, ProductNotFoundError) as exc:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(exc.args[0])
            logger.error('not_found_error, exc={!r}'.format(exc))
            return Response()

        response = Response(pct=discount['pct'], value_in_cents=discount['value_in_cents'])
        logger.info('response_sent, response={!r}'.format(response))
        return response

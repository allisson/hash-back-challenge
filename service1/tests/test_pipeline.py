from datetime import date
from unittest import mock

import pytest
from freezegun import freeze_time

from service1.exceptions import ProductNotFoundError, UserNotFoundError
from service1.pipeline import DiscountPipeline


@pytest.fixture
def discount_pipeline():
    return DiscountPipeline('user_id', 'product_id', mock.Mock())


def test_discount_pipeline_user_not_found(discount_pipeline):
    discount_pipeline.database_manager.fetch_user.return_value = {}

    with pytest.raises(UserNotFoundError) as excinfo:
        discount_pipeline.run()
    assert str(excinfo.value) == 'user_not_found, user_id=user_id'


def test_discount_pipeline_product_not_found(discount_pipeline, user):
    discount_pipeline.database_manager.fetch_user.return_value = user
    discount_pipeline.database_manager.fetch_product.return_value = {}

    with pytest.raises(ProductNotFoundError) as excinfo:
        discount_pipeline.run()
    assert str(excinfo.value) == 'product_not_found, product_id=product_id'


@freeze_time("2019-01-01")
def test_discount_pipeline_without_discount(discount_pipeline, user, product):
    discount_pipeline.database_manager.fetch_user.return_value = user
    discount_pipeline.database_manager.fetch_product.return_value = product

    discount = discount_pipeline.run()
    assert discount == {'pct': 0, 'value_in_cents': 5000}


@freeze_time("2019-02-28")
def test_discount_pipeline_with_birthday_discount(discount_pipeline, user, product):
    discount_pipeline.database_manager.fetch_user.return_value = user
    discount_pipeline.database_manager.fetch_product.return_value = product

    discount = discount_pipeline.run()
    assert discount == {'pct': 0.05, 'value_in_cents': 4750}


@freeze_time("2019-11-25")
def test_discount_pipeline_with_blackfriday_discount(discount_pipeline, user, product):
    discount_pipeline.database_manager.fetch_user.return_value = user
    discount_pipeline.database_manager.fetch_product.return_value = product

    discount = discount_pipeline.run()
    assert discount == {'pct': 0.1, 'value_in_cents': 4500}


@freeze_time("2019-11-25")
def test_discount_pipeline_with_blackfriday_and_birthday_discount(discount_pipeline, user, product):
    user['date_of_birth'] = date(1985, 11, 25)
    discount_pipeline.database_manager.fetch_user.return_value = user
    discount_pipeline.database_manager.fetch_product.return_value = product

    discount = discount_pipeline.run()
    assert discount == {'pct': 0.1, 'value_in_cents': 4500}

from unittest import mock

import grpc
import pytest

from service1.exceptions import ProductNotFoundError, UserNotFoundError
from service1.pb.discount_pb2 import Request, Response
from service1.service import DiscountService


@mock.patch('service1.service.DiscountPipeline')
@pytest.mark.parametrize('exception', [
    UserNotFoundError('user_not_found, user_id=user_id'),
    ProductNotFoundError('product_not_found, product_id=product_id'),

])
def test_discount_service_fetch_with_not_found_exception(mock_discount_pipeline, exception):
    service = DiscountService()
    mock_discount_pipeline.return_value.run.side_effect = exception
    context = mock.Mock()
    request = Request(product_id='product_id', user_id='user_id')

    result = service.Fetch(request, context)
    assert result == Response()
    context.set_code.assert_called_once_with(grpc.StatusCode.NOT_FOUND)
    context.set_details.assert_called_once_with(exception.args[0])


@mock.patch('service1.service.DiscountPipeline')
def test_discount_service_fetch(mock_discount_pipeline):
    service = DiscountService()
    mock_discount_pipeline.return_value.run.return_value = {'pct': 0.1, 'value_in_cents': 4500}
    context = mock.Mock()
    request = Request(product_id='product_id', user_id='user_id')

    result = service.Fetch(request, context)
    assert result == Response(pct=0.1, value_in_cents=4500)
    context.set_code.assert_not_called()
    context.set_details.assert_not_called()

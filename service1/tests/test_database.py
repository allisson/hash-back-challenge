import os

import pytest

from .fixtures import create_fixtures, drop_fixtures
from service1.database import DatabaseManager


@pytest.fixture
def database_manager():
    return DatabaseManager(database_url=os.environ.get('TEST_DATABASE_URL'))


def test_database_manager_fetch_product(database_manager, product, user):
    create_fixtures(database_manager.db, product, user)

    product_from_db = database_manager.fetch_product(product['id'])
    assert all(
        product_from_db[field] == product[field] for field in ('id', 'title', 'description', 'price_in_cents')
    )

    product_not_found = database_manager.fetch_product('not-found-id')
    assert product_not_found == {}

    drop_fixtures(database_manager.db, product, user)


def test_database_manager_fetch_user(database_manager, product, user):
    create_fixtures(database_manager.db, product, user)

    user_from_db = database_manager.fetch_user(user['id'])
    assert all(
        user_from_db[field] == user[field] for field in ('id', 'first_name', 'last_name', 'date_of_birth')
    )

    user_not_found = database_manager.fetch_product('not-found-id')
    assert user_not_found == {}

    drop_fixtures(database_manager.db, product, user)

def create_fixtures(db, product, user):
    db.query(
        '''
        INSERT INTO products (id, title, description, price_in_cents, created_at)
        VALUES(:id, :title, :description, :price_in_cents, :created_at)
        ''',
        **product
    )
    db.query(
        '''
        INSERT INTO users (id, first_name, last_name, date_of_birth, created_at)
        VALUES(:id, :first_name, :last_name, :date_of_birth, :created_at)
        ''',
        **user
    )


def drop_fixtures(db, product, user):
    db.query(
        '''
        DELETE FROM products
        WHERE id = :id
        ''',
        id=product['id']
    )
    db.query(
        '''
        DELETE FROM users
        WHERE id = :id
        ''',
        id=user['id']
    )

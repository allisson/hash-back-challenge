from datetime import date, datetime
from uuid import uuid4

import pytest


@pytest.fixture
def product():
    return {
        'id': str(uuid4()),
        'title': 'My Product',
        'description': 'My Description',
        'price_in_cents': 5000,
        'created_at': datetime.utcnow(),
    }


@pytest.fixture
def user():
    return {
        'id': str(uuid4()),
        'first_name': 'John',
        'last_name': 'Connor',
        'date_of_birth': date(1985, 2, 28),
        'created_at': datetime.utcnow(),
    }
